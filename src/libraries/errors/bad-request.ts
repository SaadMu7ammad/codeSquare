import { CustomAPIError } from './custom-api';

class BadRequestError extends CustomAPIError {
  public statusCode: number;
  constructor(message: string) {
    console.log('BadRequestError');
    super(message, 500);
    this.statusCode = 500;
  }
}
export { BadRequestError };
