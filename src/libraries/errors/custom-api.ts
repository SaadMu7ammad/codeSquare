class CustomAPIError extends Error {
  public status: string;
  public code?: number;
  public keyValue?: Record<string, any>;
  public value?:Record<string, any>; 

  constructor(public message: string, public statuscode: number) {
    console.log('CustomAPIError');
    super(message);
    this.statuscode = statuscode;
    this.status = `${statuscode}`.startsWith('4') ? 'error' : 'fail';
  }
  // constructor(message: string) {
  //   super(message);
  // }
}

export { CustomAPIError };
// export default CustomAPIError;
