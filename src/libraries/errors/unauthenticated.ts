import { CustomAPIError } from './custom-api';

class UnauthenticatedError extends CustomAPIError {
  statusCode: number;
  constructor(message: string) {
    super(message, 401);
    this.statusCode = 401;
  }
}

export { UnauthenticatedError };
