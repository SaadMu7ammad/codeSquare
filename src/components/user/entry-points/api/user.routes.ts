import { auth } from '../../../../middlewares/auth';
import { userUseCase } from '../../domain/user.use_case';
import express from 'express';

export default function defineRoutes(expressApp: express.Application) {
  const router = express.Router();

  router.post('/register', async (req, res, next) => {
    try {
      console.log(`Auth API was called to register User`);
      const registerResponse = await userUseCase.register(req, res, next);
      return res.json(registerResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });

  router.post('/login', async (req, res, next) => {
    try {
      console.log(`Auth API was called to auth User`);
      const loginResponse = await userUseCase.login(req, res, next);
      return res.json(loginResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.post('/logout', async (req, res, next) => {
    try {
      console.log(`Auth API was called to logout User`);
      const logoutResponse = await userUseCase.logout(req, res, next);
      return res.json(logoutResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.get('/profile', auth, (req, res, next) => {
    try {
      console.log(`API was called to get profile`);
      const showProfileResponse = userUseCase.showProfile(req, res, next);
      return res.json(showProfileResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.put('/update-profile', auth, async (req, res, next) => {
    try {
      console.log(`API was called to update profile`);
      const updateProfileResponse = await userUseCase.updateUser(
        req,
        res,
        next
      );
      return res.json(updateProfileResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  expressApp.use('/v1/api/users', router);
}
