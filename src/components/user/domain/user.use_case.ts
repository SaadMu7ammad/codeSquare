import { NextFunction, Request, RequestHandler, Response } from 'express';
import { userService } from './user.service';
import { IUserDocument, updateUserData } from '../data-access/models/user.interface';
import { RequestWithBody } from '../../../utils/types/request.interface';
import { modifiedResponse } from '../../../utils/types/response.interface';
export type ExpressHandler<Req, Res> = RequestHandler<
  string,
  Partial<Res>,
  Partial<Req>,
  any
>;
/* Handles user Register.
 * @function
 * @async
 * @param {Request} req - Express request object.
 * @param {Response} res - Express response object.
 * @param {NextFunction} next - Express next middleware function.
 * @returns {Promise<Response>} A promise that resolves once the response is sent.
 * @throws {CustomAPIErrorError} If there is an issue during the Register process.
 */

const register: RequestHandler = async (req, _res, _next): Promise<Partial<IUserDocument>> => {
  const { userName, email, password, firstName, lastName }: { userName: string, email: string, password: string, firstName: string, lastName: string } = req.body;
  const user = await userService.registerService(
    userName,
    email,
    password,
    firstName,
    lastName
  );
  return user;
};
/**
 * Handles user login.
 * @function
 * @async
 * @param {Request} req - Express request object.
 * @param {Response} res - Express response object.
 * @param {NextFunction} next - Express next middleware function.
 * @returns {Promise<Response>} A promise that resolves once the response is sent.
 * @throws {CustomAPIErrorError} If there is an issue during the Register process.
 */
export const login = async (
  req: RequestWithBody,
  res: Response,
  _next: NextFunction
): Promise<modifiedResponse> => {
  const { email, password }: { email: string, password: string } = req.body;
  const user = await userService.loginService(email, password, res);
  return user;
};
/**
 * Handles user logout.
 * @function
 * @async
 * @param {Request} req - Express request object.
 * @param {Response} res - Express response object.
 * @param {NextFunction} next - Express next middleware function.
 * @returns {Promise<Response>} A promise that resolves once the response is sent.
 * @throws {Error} If there is an issue during the login process.
 */
export const logout: RequestHandler = async (
  _req,
  res,
  _next
): Promise<modifiedResponse> => {
  userService.logoutService(res);
  return { msg: 'logout' };
};

const showProfile = (_req: Request, res: Response, _next: NextFunction) => {
  const storedUser:IUserDocument=res.locals.user
  const userProfile = userService.showProfileService(storedUser);
  return userProfile;
};
const updateUser = async (
  req: Request,
  res: Response,
  _next: NextFunction
) => {
  const dataInput: updateUserData = {
    email: req.body.email,
    userName: req.body.userName,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: req.body.password
  };
  const storedUser = res.locals.user;
  const updateUserProfile = await userService.updateProfileService(
    dataInput,
    storedUser
  );
  return updateUserProfile;
};
export const userUseCase = {
  register,
  login,
  logout,
  showProfile,
  updateUser,
};
