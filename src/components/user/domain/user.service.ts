import { Response } from 'express';
import * as bcrypt from 'bcrypt';
import crypto from 'crypto';
import { UserDataStore } from '../data-access/user.dao';
import {
  IUser,
  IUserDocument,
  updateUserData,
} from '../data-access/models/user.interface';
import {
  BadRequestError,
  UnauthenticatedError,
} from '../../../libraries/errors/index';
import { generateAccessToken } from '../../../utils/generateToken';
import { modifiedResponse } from '../../../utils/types/response.interface';
import { tokenSchema } from '../../../utils/types/auth.interface';
import { configENV } from '../../../config';
const loginService = async (
  email: string,
  password: string,
  res: Response
): Promise<modifiedResponse> => {
  if (!email || !password) {
    throw new BadRequestError('empty inputs');
  }

  const userObj = new UserDataStore();

  const user = await userObj.getUserByEmail(email);
  if (!user) {
    throw new UnauthenticatedError('email not found');
  }
  const isMatch = await bcrypt.compare(password, user.password as string);
  if (!isMatch) {
    throw new UnauthenticatedError('incorrect password');
  }
  let tokenSet: tokenSchema = {
    user: { name: user.userName, userId: user.id.toString() },
  }
  const token = generateAccessToken(tokenSet, res);
  return {
    user: {
      _id: user._id.toString(),
      userId: user.userId.toString(),
      userName: user.userName,
      email: user.email,
    },
    token: token,
  };
};

const registerService = async (
  userName: string,
  email: string,
  password: string,
  firstName?: string,
  lastName?: string
): Promise<IUserDocument> => {
  if (!userName || !email || !password)
    throw new BadRequestError('empty inputs');
  const userObj = new UserDataStore();
  const data: IUser = {
    userId: crypto.randomUUID(),
    userName: userName,
    email: email,
    password: password,
    firstName: firstName,
    lastName: lastName,
    posts: new Array(),
  };
  const emailIsDupliacated = await userObj.getUserByEmail(email);
  if (emailIsDupliacated) throw new BadRequestError('email is used');

  const userNameIsDupliacated = await userObj.getUserByUsername(userName);
  if (userNameIsDupliacated) throw new BadRequestError('username is used');

  //hashing the password
  const salt = await bcrypt.genSalt(+configENV.getsalt());
  const hashed = await bcrypt.hash(data.password, salt);
  if (!hashed) throw new BadRequestError('password not hashed plz try again');
  data.password = hashed;

  const user = await userObj.createUser(data);
  if (!user) throw new BadRequestError('user not created .. please try again');
  return user;
};

export const logoutService = async (res: Response): Promise<Response> => {
  return res.cookie('jwt', '', {
    httpOnly: true,
    expires: new Date(0),
  });
};

export const showProfileService = (reqBody: IUserDocument): IUserDocument => {
  return reqBody;
};
export const updateProfileService = async (
  reqBody: updateUserData,
  oldUser: IUserDocument
): Promise<IUserDocument| undefined> => {
  const userObj = new UserDataStore();
  if (reqBody.password) {//hashing the password
    const salt = await bcrypt.genSalt(+configENV.getsalt());
    const hashed = await bcrypt.hash(reqBody.password, salt);
    if (!hashed) throw new BadRequestError('password not hashed plz try again');
    reqBody.password = hashed;
  }
  const updatedUser = await userObj.updateCurrentUser(oldUser._id, reqBody);
  // oldUser.userName = 'tst';
  // await oldUser.save()
  return updatedUser;
};
export const userService = {
  registerService,
  loginService,
  logoutService,
  showProfileService,
  updateProfileService,
};
