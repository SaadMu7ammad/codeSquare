import { IUser, IUserDocument } from './models/user.interface';
import UserModel from './models/user.model';
import { UserRepository } from './user.repostitory';

export class UserDataStore implements UserRepository {
  async createUser(
    dataUser: IUser
  ): Promise<IUserDocument | undefined> {
    const user = await UserModel.create(dataUser);
    if (!user) return undefined;
    return user;
  }
  async getUserByEmail(
    email: string
  ): Promise<IUserDocument | undefined> {
    const user = await UserModel.findOne({ email:email });
    if (!user) return undefined;

    return user;
  }
  async getUserByUsername(
    userName: string
  ): Promise<IUserDocument | undefined> {
    const user = await UserModel.findOne({ userName:userName });
    if (!user) return undefined;

    return user;
  }
  async updateCurrentUser(
    id: string,
    newUser: Partial<IUser>
  ): Promise<IUserDocument | undefined> {
    const user = await UserModel.findOneAndUpdate({ _id: id }, newUser, {
      new: true,
    });
    if (!user) return undefined;
    return user;
  }
  async getUserById(_id: string): Promise<IUserDocument | undefined> {
    throw new Error('Method not implemented.');
  }
}
