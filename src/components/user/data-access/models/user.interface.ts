import { Document } from "mongoose";
import { IPostDocument } from "../../../post/data-access/models/post.interface";
export interface IUser {
  userId: string;
  firstName?: string|null|undefined;
  lastName?: string|null|undefined;
  userName: string;
  email: string;
  password: string;
  posts:Array<IPostDocument['_id']>;
}
export interface IUserDocument extends IUser,Document {}

export interface updateUserData {
  firstName?: string;
  lastName?: string;
  userName?: string;
  email?: string;
  password?: string;
}
