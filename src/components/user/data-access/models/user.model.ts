import mongoose, { Schema } from 'mongoose';
// import * as bcrypt from 'bcrypt';
// import { NextFunction } from 'express';
// import { CustomAPIError } from '../../../../libraries/errors';
// import { IUserDocument } from './user.interface';
const userSchema = new Schema({
  userId: { type: String, required: true, unique: true },
  firstName: { type: String },
  lastName: { type: String },
  userName: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  posts: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
});

// userSchema.pre('save',async function (this: IUserDocument, next: NextFunction) {
//     try {
//       if (!this.isModified('password')) {
//         //to not change password every time we edit the user data
//         console.log('password not changed');
//         return next();
//       } else {
//         const salt = await bcrypt.genSalt(10);
//         this.password = await bcrypt.hash(this.password, salt);
//         console.log('password has been changed');
//         next();
//       }
//     } catch (err) {
//       // err.message = 'problem while handling the password';
//       // next(err);
//       const error = new CustomAPIError(
//         'problem while handling the password',
//         404
//       );
//       next(error);
//     }
//   }
// );
// userSchema.pre('findOneAndUpdate', async function (this:IUserDocument,next:NextFunction) {
//   try {
//     // the update operation object is stored within this.getUpdate()
//     const updated = this.getUpdate() as IUser;
//     if (updated.password) {
//       const salt = await bcrypt.genSalt(10);
//       updated.password = await bcrypt.hash(updated.password, salt);
//       console.log('password changed successfully');
//     } else {
//       console.log('password not changed');
//     }
//   } catch (err) {
//     const error = new CustomAPIError('problem while handling the password',404)
//     // err as typeof error;
//     // err.message = 'problem while handling the password';
//     next(error);
//   }
// });
const UserModel = mongoose.model('User', userSchema);
export default UserModel;
