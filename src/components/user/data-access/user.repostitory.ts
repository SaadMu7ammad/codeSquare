import { IUser, IUserDocument } from './models/user.interface';
import UserModel from './models/user.model';

export interface UserRepository {
  createUser(user: IUser): Promise<Partial<IUserDocument> | undefined>;
  updateCurrentUser(
    id: string,
    newUser: Partial<IUser>
  ): Promise<Partial<IUserDocument> | undefined>;
  getUserById(id: string): Promise<Partial<IUserDocument> | undefined>;
  getUserByEmail(email: string): Promise<Partial<IUserDocument> | undefined>;
  getUserByUsername(
    userName: string
  ): Promise<Partial<IUserDocument> | undefined>;
  updateCurrentUser(
    id: string,
    newUser: Partial<IUser>
  ): Promise<Partial<IUserDocument> | undefined>;
}
