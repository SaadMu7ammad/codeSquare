import express from 'express';
import { auth } from '../../../../middlewares/auth';
import { commentUseCase } from '../../domain/comment.use_case';

export default function defineRoutes(expressApp: express.Application) {
  const router = express.Router();

  router.post('/create/:id', auth, async (req, res, next) => {
    try {
      console.log(`Authed API was called to create a comment`);
      const commentCreatedResponse = await commentUseCase.createComment(
        req,
        res,
        next
      );
      return res.json(commentCreatedResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  //   router.get('/',  async (req, res, next) => {
  //     try {
  //     } catch (error) {
  //     }
  //   });
  router.put('/edit/:id', auth, async (req, res, next) => {
    try {
      console.log(`Authed API was called to edit a post`);
      const editedPostResponse = await commentUseCase.editComment(
        req,
        res,
        next
      );
      return res.json(editedPostResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.delete('/del/:id', auth, async (req, res, next) => {
    try {
      console.log(`Authed API was called to edit a post`);
      const delPostResponse = await commentUseCase.delComment(req, res, next);
      return res.json(delPostResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });

  expressApp.use('/v1/api/post/comments', router);
}
