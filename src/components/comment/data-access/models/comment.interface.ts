import { IPostDocument } from "../../../post/data-access/models/post.interface";
import { IUserDocument } from "../../../user/data-access/models/user.interface";
import { Document } from "mongoose";

  export interface IComment {
    userId: IUserDocument['_id'];
    postId: IPostDocument['_id'];
    comment: string;
    postedAt: number;
    liked?: boolean|null;
}
  
export interface ICommentDocument extends IComment,Document {}

export interface ICommentDocumentResponse {
  comment:ICommentDocument
}
