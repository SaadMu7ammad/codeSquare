import mongoose, { Schema } from 'mongoose';

// Define the Mongoose schema for Comment
const commentSchema= new Schema({
  userId: { type: Schema.Types.ObjectId,ref: 'User',required:true } ,
  postId: { type: Schema.Types.ObjectId,ref: 'Post',required:true },
  comment: { type: String, required: true },
  postedAt: { type: Number, required: true },
  liked: { type: Boolean },
});

// Create the Mongoose model
const CommentModel = mongoose.model('Comment', commentSchema);

export default CommentModel;
