import { CommentRepository } from './comment.dao';
import { IComment, ICommentDocument } from './models/comment.interface';
import CommentModel from './models/comment.model';

export class CommentDataStore implements CommentRepository {
  async createComment(
    comment: IComment
  ): Promise<ICommentDocument | null> {
    const newComment = await CommentModel.create(comment)
    return newComment;
  }
  countComments(_postId: string): Promise<number> {
    throw new Error('Method not implemented.');
  }
  listComments(_postId: string): Promise<ICommentDocument[]> {
    throw new Error('Method not implemented.');
  }
  async deleteComment(id: string): Promise<ICommentDocument | null> {
    const newComment = await CommentModel.findByIdAndDelete(id)
    return newComment;
  }
  async getComment(id: string): Promise<ICommentDocument | null> {
    const comment = await CommentModel.findById(id)
    return comment;
  }
}
