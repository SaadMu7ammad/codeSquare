import { IComment, ICommentDocument } from "./models/comment.interface";


export interface CommentRepository {
    createComment(comment: IComment): Promise<ICommentDocument|null>;
    countComments(postId: string): Promise<number>;
    listComments(postId: string): Promise<IComment[]>;
    deleteComment(id: string): Promise<ICommentDocument|null>;
  }