import {
  IUserDocument,
} from 'components/user/data-access/models/user.interface';

import { BadRequestError, NotFoundError } from '../../../libraries/errors';
import {
  IComment,
  ICommentDocument,
} from '../data-access/models/comment.interface';
import { CommentDataStore } from '../data-access/comment.repostitory';
import { PostDataStore } from '../../post/data-access/post.dao';
import { commentUtils } from './comment.utils';

const createComment = async (
  reqBody: IComment,
  _user: IUserDocument
): Promise<ICommentDocument> => {
  if (!reqBody.comment)
    throw new BadRequestError('must include content...try again');

  //check the user post
  const postDao = new PostDataStore();
  const post= await postDao.getPostById(reqBody.postId, '');
  if (!post) throw new NotFoundError('post not found...try again');

  const commentDao = new CommentDataStore();
  const createdComment = await commentDao.createComment(reqBody);
  if (!createdComment)
    throw new BadRequestError('comment not added...try again');

  await commentUtils.addCommentToPostcomments(post, createdComment._id)
  
  return createdComment;
};
const editComment = async (
  reqBody: ICommentDocument,
  user: IUserDocument
): Promise<ICommentDocument> => {
  if (!reqBody.comment)
    throw new BadRequestError('must include content to update...try again');

  const commentDao = new CommentDataStore();
  const comment= await commentDao.getComment(reqBody._id);
  if (!comment) throw new NotFoundError('comment not found...try again');
  //check the comment owner
  if (comment.userId.toString() !== user._id.toString()) {
    throw new NotFoundError('you cant edit that comment');
  }

  await commentUtils.editComment(reqBody.comment,comment)

  return comment;
};
const delComment = async (
  reqBody: ICommentDocument,
  user: IUserDocument
): Promise<string> => {
  const commentDao = new CommentDataStore();
  const comment = await commentDao.getComment(reqBody._id);
  if (!comment) throw new NotFoundError('comment not found...try again');
  //check the comment owner
  if (comment.userId.toString() !== user._id.toString()) {
    throw new NotFoundError('you cant edit that comment');
  }

  const idDeleted = await commentDao.deleteComment(reqBody._id);
  if (!idDeleted) throw new NotFoundError('comment not deleted...try again');
  //remove the id from the comments array linked to the post
  const postDao = new PostDataStore();
  const post = await postDao.getPostById(
    comment.postId,
    'title url liked comments likes'
  );
  if (!post) throw new NotFoundError('post not found');

  await commentUtils.delCommentIdFromPostComments(post,comment,false)
  return 'comment deleted successfully';
};
export const commentService = {
  createComment,
  editComment,
  delComment,
};
