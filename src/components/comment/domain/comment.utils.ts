import { IPostDocument } from '../../post/data-access/models/post.interface';
import { ICommentDocument } from '../data-access/models/comment.interface';

const addCommentToPostcomments = async (
  post: IPostDocument,
  commentId: string
) => {
  post.comments.push(commentId); //add comment id to the post arr comments
  await post.save();
};
const editComment = async (newText: string, comment: ICommentDocument) => {
  comment.comment = newText;
  await comment.save();
};
const delCommentIdFromPostComments = async (
    post: IPostDocument,
    comment: ICommentDocument,
    flagRefId: boolean
  ) => {
    for (const [index, idPost] of post.comments.entries()) {
      if (idPost.toString() === comment._id.toString()) {
        flagRefId = true;
        post.comments.splice(index, 1); // Remove the idPost
        break; // Exit the loop early
      }
    }
    if (flagRefId) {
      await post.save();
    }
  };
export const commentUtils = {
  addCommentToPostcomments,
  editComment,delCommentIdFromPostComments
};
