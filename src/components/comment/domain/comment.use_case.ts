import { NextFunction, Request, Response } from 'express';
import {
  IComment,
  ICommentDocument,
  ICommentDocumentResponse,
} from '../data-access/models/comment.interface';
import { commentService } from './comment.service';

const createComment = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<ICommentDocumentResponse> => {
  const storedUser = res.locals.user;
  req.body.postedAt = Date.now(); //add the date
  req.body.userId = storedUser._id; //link the id of user to the post
  req.body.postId = req.params.id;
  const data: IComment = req.body;

  const newComment = await commentService.createComment(data, storedUser);

  return { comment: newComment };
};
const editComment = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<ICommentDocumentResponse> => {
  const storedUser = res.locals.user;
  req.body._id = req.params.id; //the id of the comment
  const data: ICommentDocument = req.body;
  const editedComment = await commentService.editComment(data, storedUser);

  return { comment: editedComment };
};
const delComment = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<{message:string}> => {
  const storedUser = res.locals.user;
  req.body._id = req.params.id; //the id of the comment
  const data: ICommentDocument = req.body;
  const deletedComment = await commentService.delComment(data, storedUser);

  return {message:deletedComment};
};
export const commentUseCase = {
  createComment,
  editComment,
  delComment,
  //   getAllPosts,
  //   getPostById,delPostById
};
