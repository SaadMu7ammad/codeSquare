import {
  IPostDocument,
  IPostDocumentResponse,
  IQuery,
} from '../data-access/models/post.interface';
import { PostDataStore } from '../data-access/post.dao';
import { BadRequestError, NotFoundError } from '../../../libraries/errors';
import { IUserDocument } from '../../user/data-access/models/user.interface';
import { postUtils } from './post.utils';

const createPost = async (
  reqBody: IPostDocument,
  user: IUserDocument
): Promise<IPostDocumentResponse> => {
  if (!reqBody.title)
    throw new BadRequestError('must include title...try again');
  const postDao = new PostDataStore();
  const createdPost = await postDao.createPost(reqBody);
  if (!createdPost) throw new BadRequestError('post not created...try again');
  postUtils.addPostToUserPosts(user, createdPost._id);
  postUtils.createUrlToPost('http://localhost:3000/v1/api/posts/', createdPost);
  return { post: createdPost };
};

const getAllPosts = async (
  reqQuery: IQuery
): Promise<{ posts: IPostDocument[] | null; page: number }> => {
  let { page, size, sort } = reqQuery;
  // If the page is not applied in query.
  if (!page) {
    // Make the Default value one.
    page = "1";
  }
  if (!size) {
    size = "2";
  }
  if (!sort) {
    sort = '_id';
  }
  const postDao = new PostDataStore();
  const posts = await postDao.getAllPosts(page, size,sort);
  if(!posts.posts)return {posts:null,page:posts.page}
  return {posts:posts.posts,page:posts.page};
};
const getPostById = async (id: string | undefined): Promise<IPostDocument> => {
  if (!id) throw new NotFoundError('no id included');
  const postDao = new PostDataStore();
  const post = await postDao.getPostById(id, 'title url liked comments likes');
  if (!post) throw new NotFoundError('post not found');
  return post;
};
const delPostById = async (
  id: string | undefined | null,
  user: IUserDocument
): Promise<string> => {
  if (!id) throw new NotFoundError('no id included');
  //check the post is created to the user or not
  const post = await postUtils.checkPostIsExist(id);

  if (post.userId.toString() !== user._id.toString())
    throw new NotFoundError('cant delete a post not created by yourself');

  const postIsDeleted = await postUtils.delPost(id);

  await postUtils.delCommentsLinkedToDelPost(postIsDeleted);
  await postUtils.delLikesLinkedToDelPost(postIsDeleted);

  await postUtils.delPostIdFromUserPosts(user, postIsDeleted, false);

  return 'post deleted successfully';
};
export const postService = {
  createPost,
  getAllPosts,
  getPostById,
  delPostById,
};
