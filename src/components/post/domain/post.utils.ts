import { CommentDataStore } from '../../comment/data-access/comment.repostitory';
import { NotFoundError } from '../../../libraries/errors';
import { IUserDocument } from '../../user/data-access/models/user.interface';
import { IPostDocument } from '../data-access/models/post.interface';
import { PostDataStore } from '../data-access/post.dao';
import { likeDataStore } from '../../like/data-access/like.dao';

const addPostToUserPosts = async (user: IUserDocument, postId: string) => {
  user.posts.push(postId); //add post id to the user arr posts
  await user.save();
};
const createUrlToPost = async (link: string, post: IPostDocument) => {
  //`http://localhost:3000/v1/api/posts/${createdPost._id}`; //each post has a unique id and that is the url
  post.url = link + post._id.toString();
  await post.save();
};

const checkPostIsExist = async (id: string) => {
  const postDao = new PostDataStore();
  const post = await postDao.getPostById(id, '');
  if (!post) throw new NotFoundError('post not found');
  return post;
};

const delPost = async (id: string) => {
  const postDao = new PostDataStore();
  const post = await postDao.deletePost(id);
  if (!post) throw new NotFoundError('post not found');
  return post;
};
const delCommentsLinkedToDelPost = async (postIsDeleted: IPostDocument) => {
  const commnetDao = new CommentDataStore();
  const commentPromises = postIsDeleted.comments.map(
    async (idComment, _indx) => {
      return await commnetDao.deleteComment(idComment);
    }
  );
  // Wait for all asynchronous operations to complete
  await Promise.all(commentPromises); //delete all comments linked to the post
};
const delLikesLinkedToDelPost = async (postIsDeleted: IPostDocument) => {
    const likeDao = new likeDataStore();
    const likePromises = postIsDeleted.likes.map(
      async (idLike, _indx) => {
        return await likeDao.removeLike(idLike);
      }
    );
    // Wait for all asynchronous operations to complete
    await Promise.all(likePromises); //delete all likes linked to the post
  };
const delPostIdFromUserPosts = async (
  user: IUserDocument,
  post: IPostDocument,
  flagRefId: boolean
) => {
  for (const [index, idPost] of user.posts.entries()) {
    if (idPost.toString() === post._id.toString()) {
      flagRefId = true;
      user.posts.splice(index, 1); // Remove the idPost
      break; // Exit the loop early
    }
  }
  if (flagRefId) {
    await user.save();
  }
};
export const postUtils = {
  addPostToUserPosts,
  createUrlToPost,
  checkPostIsExist,
  delPost,
  delCommentsLinkedToDelPost,
  delPostIdFromUserPosts,delLikesLinkedToDelPost
};
