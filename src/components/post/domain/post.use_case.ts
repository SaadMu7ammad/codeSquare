import { NextFunction, Response, Request } from 'express';
import {
  IPostDocument,
  IPostDocumentResponse,
  queryRequest,
} from '../data-access/models/post.interface';
import { postService } from './post.service';

const createPost = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<IPostDocumentResponse> => {
  const storedUser = res.locals.user;
  req.body.postedAt = Date.now(); //add the date
  req.body.userId = storedUser._id; //link the id of user to the post
  const data: IPostDocument = req.body;

  const newPost = await postService.createPost(data, storedUser);

  return { post: newPost.post };
};

const getAllPosts = async (
  _req: Request,
  _res: Response,
  _next: NextFunction
): Promise<{ posts: IPostDocument[] | null; page: number }>=> {
  const req = _req as queryRequest;
  const qParamters = {
    page: req.query.page,
    size: req.query.size,
    sort: req.query.sort,
  };
  const allPosts = await postService.getAllPosts(qParamters);
  return allPosts;
};
const getPostById = async (
  req: Request,
  _res: Response,
  _next: NextFunction
): Promise<IPostDocument> => {
  const { id } = req.params;
  const post = await postService.getPostById(id);
  return post;
};
const delPostById = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<{ message: string }> => {
  const { id } = req.params;
  const storedUser = res.locals.user;
  const message = await postService.delPostById(id, storedUser);
  return { message: message };
};
export const postUseCase = {
  createPost,
  getAllPosts,
  getPostById,
  delPostById,
};
