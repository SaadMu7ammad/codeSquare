import {  IPostDocument } from './models/post.interface';
export interface PostRepository {
  listPosts(userId?: string): Promise<IPostDocument[]>;
  createPost(post: IPostDocument): Promise<IPostDocument | null>;
  getPost(id: string, userId?: string): Promise<IPostDocument | null>;
  getAllPosts(page: string,size: string,sort: string): Promise<{ posts: IPostDocument[] | null; page: number }>;
  getPostByUrl(url: string): Promise<IPostDocument | null>;
  getPostById(url: string,selection:string): Promise<IPostDocument | null>;
  getPostByTag(url: string): Promise<IPostDocument | null>;
  deletePost(id: string): Promise<IPostDocument|null> ;
}
