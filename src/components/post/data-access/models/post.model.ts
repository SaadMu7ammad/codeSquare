// post.model.ts
import mongoose, { Schema } from 'mongoose';

// Define the Mongoose schema for Post
const postSchema = new Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  url: { type: String, unique: true },
  userId: { type: Schema.Types.ObjectId, required: true },
  postedAt: { type: Number, required: true },
  liked: { type: Boolean },
  comments: [{ type:Schema.Types.ObjectId,ref: 'Comment'  }],
  likes: [{ type:Schema.Types.ObjectId,ref: 'Like' }]
});

// Create the Mongoose model
const PostModel = mongoose.model('Post', postSchema);

export default PostModel;
