import { IUserDocument } from '../../../user/data-access/models/user.interface';
import { Document } from 'mongoose';
import { ICommentDocument } from '../../../comment/data-access/models/comment.interface';
import { IlikeDocument } from 'components/like/data-access/models/like.interface';
import { Request } from 'express';
import { ParsedQs } from 'qs';

export interface IPost {
  title: string;
  content: string;
  url?: string | null;
  userId: IUserDocument['_id'];
  postedAt: number;
  liked?: boolean | null;
  // comments?: [];
  // comments: mongoose.Type s.ObjectId[];
  comments: Array<ICommentDocument['_id']>;
  likes: Array<IlikeDocument['_id']>;
}
export interface IPostDocument extends IPost, Document {}

export interface IPostDocumentResponse {
  post: IPostDocument | null;
}

export interface queryRequest extends Request {
  query: ParsedQs & { page: string; size: string; sort: string | '' };
}
export interface IQuery extends ParsedQs {
  page: string;
  size: string;
  sort: string | '';
}
