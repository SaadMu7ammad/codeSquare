import { IPostDocument } from './models/post.interface';
import PostModel from './models/post.model';
import { PostRepository } from './post.repostitory';

export class PostDataStore implements PostRepository {
  listPosts(_userId?: string): Promise<IPostDocument[]> {
    throw new Error('Method not implemented.');
  }
  async createPost(post: IPostDocument): Promise<IPostDocument | null> {
    const newPost = (await PostModel.create(post));
    return newPost;
  }
  getPost(_id: string, _userId?: string): Promise<IPostDocument> {
    throw new Error('Method not implemented.');
  }
  async getAllPosts(
    page: string,
    size: string,
    sort: string
  ): Promise<{ posts: IPostDocument[] | null; page: number }> {
    const posts = (await PostModel.find()
      .sort(sort)
      .limit(+size)
      .skip((+page - 1) * +size)
      .select('title url liked comments likes')
      .exec());
    const pageNumber = parseInt(page)
    if (!posts) return { posts: null, page: pageNumber };
    return { posts, page: pageNumber };
  }
  getPostByUrl(_url: string): Promise<IPostDocument> {
    throw new Error('Method not implemented.');
  }
  async getPostById(
    id: string,
    selection: string
  ): Promise<IPostDocument | null> {
    const post = await PostModel.findById(id).select(
      selection
    )
    return post;
  }
  getPostByTag(_url: string): Promise<IPostDocument> {
    throw new Error('Method not implemented.');
  }
  async deletePost(id: string): Promise<IPostDocument | null> {
    const post = await PostModel.findByIdAndDelete(id);
    return post;
  }
}
