import express from 'express';
import { postUseCase } from '../../domain/post.use_case';
import { auth } from '../../../../middlewares/auth';

export default function defineRoutes(expressApp: express.Application) {
  const router = express.Router();

  router.post('/create', auth, async (req, res, next) => {
    try {
      console.log(`Authed API was called to create a post`);
      const postCreatedResponse = await postUseCase.createPost(req, res, next);
      return res.json(postCreatedResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.get('/',  async (req, res, next) => {
    try {
      console.log(`public API was called to get all post`);
      const postsResponse = await postUseCase.getAllPosts(req, res, next);
      return res.json(postsResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.get('/:id',  async (req, res, next) => {
    try {
      console.log(`public API was called to get a post`);
      const postResponse = await postUseCase.getPostById(req, res, next);
      return res.json(postResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  router.delete('/:id', auth, async (req, res, next) => {
    try {
      console.log(`authed API was called to get a post`);
      const postResponse = await postUseCase.delPostById(req, res, next);
      return res.json(postResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });
  expressApp.use('/v1/api/posts', router);
}
