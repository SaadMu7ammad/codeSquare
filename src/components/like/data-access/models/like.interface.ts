import { IPostDocument } from "../../../post/data-access/models/post.interface";
import { IUserDocument } from "../../../user/data-access/models/user.interface";
import { Document } from "mongoose";
export interface ILike {
  userId: IUserDocument['_id'];
  postId: IPostDocument['_id'];
}

export interface IlikeDocument extends ILike ,Document{}