import mongoose, { Schema } from 'mongoose';

const likeSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, required: true },
  postId: { type: Schema.Types.ObjectId, required: true },
});

const likeModel = mongoose.model('Like', likeSchema);

export default likeModel;
