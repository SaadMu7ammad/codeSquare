import { IlikeDocument } from "./models/like.interface";

export interface LikeRepository {
  createLike(postId:string,userId:string): Promise<IlikeDocument|null>;
  removeLike(likeId:string): Promise<IlikeDocument|null>;
  findLike(likeId:string): Promise<IlikeDocument|null>;
}
