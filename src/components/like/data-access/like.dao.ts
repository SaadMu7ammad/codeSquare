import { LikeRepository } from './like.repostitory';
import { IlikeDocument } from './models/like.interface';
import likeModel from './models/like.model';

export class likeDataStore implements LikeRepository {
  async createLike(
    postId: string,
    userId: string
  ): Promise<IlikeDocument | null> {
    const likeAdded = await likeModel.create({
      postId: postId,
      userId: userId,
    });
    return likeAdded;
  }
  async removeLike(likeId: string): Promise<IlikeDocument | null> {
    const likeRemoved = await likeModel.findByIdAndDelete(likeId);
    return likeRemoved;
    }
    async findLike(likeId: string): Promise<IlikeDocument | null> {
        const like = await likeModel.findById(likeId);
        return like;
      }
}
