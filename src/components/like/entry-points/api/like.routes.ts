import { auth } from '../../../../middlewares/auth';
import express from 'express';
import { likeUseCase } from '../../domain/like.use_case';

export default function defineRoutes(expressApp: express.Application) {
  const router = express.Router();

  router.post('/like/:id', auth, async (req, res, next) => {
    try {
      console.log(`Auth API was called to add like`);
      const likeResponse = await likeUseCase.toggleLike(req, res, next);
      return res.json(likeResponse);
    } catch (error) {
      next(error);
      return undefined;
    }
  });


  expressApp.use('/v1/api/post/reaction', router);
}
