import { NextFunction,  Request,  Response } from 'express';
import { likeService } from './like.service';
import { IlikeDocument } from '../data-access/models/like.interface';

const toggleLike = async (
  req: Request,
  res: Response,
  _next: NextFunction
): Promise<IlikeDocument|string> => {
    const { id } = req.params;//id of the post
    const storedUser = res.locals.user;

  const like = await likeService.toggleLike(id,storedUser);
  return like;
};

export const likeUseCase = {
  toggleLike,
};
