import { IPostDocument } from '../../post/data-access/models/post.interface';
import { NotFoundError } from '../../../libraries/errors';
import { likeDataStore } from '../data-access/like.dao';
import { IlikeDocument } from '../data-access/models/like.interface';

const removeLike = async (post: IPostDocument) => {
  const likeDao = new likeDataStore();

  for (const [index, likeId] of post.likes.entries()) {
    const isLikeExist = await likeDao.findLike(likeId);
    if (isLikeExist) {
      //then remove the like from both post.likes and likes document
      const removedLike = await likeDao.removeLike(likeId);
      if (!removedLike)
        throw new NotFoundError('like not removed ...try again');
      post.likes.splice(index, 1);
      await post.save();
      return { message: 'like has been removed' };
    }
  }
  return undefined;
};
const addLikeIdToPostLikes = async (
  post: IPostDocument,
  like: IlikeDocument
) => {
  post.likes.push(like._id);
  await post.save();
};

export const likeUtils = {
  removeLike,
  addLikeIdToPostLikes,
};
