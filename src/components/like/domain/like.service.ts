import { IUserDocument } from '../../user/data-access/models/user.interface';
import { NotFoundError } from '../../../libraries/errors';
import { PostDataStore } from '../../post/data-access/post.dao';
import { likeDataStore } from '../data-access/like.dao';
import { IlikeDocument } from '../data-access/models/like.interface';
import { likeUtils } from './like.utils';

const toggleLike = async (
  id: string | undefined ,
  user: IUserDocument
): Promise<IlikeDocument | string> => {
  if (!id) throw new NotFoundError('no id included');
  const likeDao = new likeDataStore();
  const postDao = new PostDataStore();
  const post = await postDao.getPostById(id, '');
  if (!post) throw new NotFoundError('post not found');

  const isLikedBefore = await likeUtils.removeLike(post);
  if (isLikedBefore) return isLikedBefore.message;

  const like = await likeDao.createLike(post._id, user._id);
  if (!like) throw new NotFoundError('like not added ...try again');
  await likeUtils.addLikeIdToPostLikes(post, like);

  return like;
};

export const likeService = {
  toggleLike,
};
