import { configENV } from '../config';
import dotenv from 'dotenv';
dotenv.config();
import mongoose from 'mongoose';

const MONGO_URL = configENV.getMONGO_URL()

const connectToDB = () => {
  mongoose.connect(MONGO_URL);
  console.log(`db is connected`);
};
//EXPORT
export default connectToDB;
