import http from 'http';
import app from './app';
import connectToDB from './config/connect.db';
const port: number = 3000;

const startApp = http.createServer(app);

const startServer = () => {
  try {
    startApp.listen(port, () => {
      console.log('server on port ' + port);
    });
    connectToDB();
  } catch (err) {
    console.log(err);
  }
};
startServer();
