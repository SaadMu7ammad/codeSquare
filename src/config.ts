function getJWT_SECRET(): string {
  const JWT_SECRET = process.env.JWT_SECRET;
  if (!JWT_SECRET) {
    console.log('mising JWT_SECRET');
    process.exit(1);
  }
  return JWT_SECRET;
}
function getJWT_LIFETIME(): string {
  const JWT_LIFETIME = process.env.JWT_LIFETIME;
  if (!JWT_LIFETIME) {
    console.log('mising JWT_LIFETIME');
    process.exit(1);
  }
  return JWT_LIFETIME;
}
function getsalt(): string {
  const salt = process.env.salt;
  if (!salt) {
    console.log('mising salt');
    process.exit(1);
  }
  return salt;
}
function getNODE_ENV(): string {
  const NODE_ENV = process.env.NODE_ENV;
  if (!NODE_ENV) {
    console.log('mising NODE_ENV');
    process.exit(1);
  }
  return NODE_ENV;
}
function getMONGO_URL(): string {
  const MONGO_URL = process.env.MONGO_URL;
  if (!MONGO_URL) {
    console.log('mising MONGO_URL');
    process.exit(1);
  }
  return MONGO_URL;
}

export const configENV = {
  getJWT_LIFETIME,
  getJWT_SECRET,
  getMONGO_URL,
  getNODE_ENV,
  getsalt
};
