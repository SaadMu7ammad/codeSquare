import express from 'express';
import morgan from 'morgan';

import { mountAllRoutes } from './mounter.apis';
import { errorHandlerMiddleware } from './middlewares/error-handler';
import { notFoundMiddleware } from './middlewares/not-found';
const app = express();
app.use(express.json()); //for json req
app.use(express.urlencoded({ extended: true })); //form data

app.use(morgan('dev'));

mountAllRoutes(app)

app.use(notFoundMiddleware);
app.use(errorHandlerMiddleware);

export default app;
