import { Application } from 'express';

import authUserRoutes from './components/user/entry-points/api/user.routes';
import postRoutes from './components/post/entry-points/api/post.routes';
import commentRoutes from './components/comment/entry-points/api/comment.routes';
import likeRoutes from './components/like/entry-points/api/like.routes';

export function mountAllRoutes(app:Application) {
  authUserRoutes(app);
  postRoutes(app);
  commentRoutes(app);
  likeRoutes(app);
}