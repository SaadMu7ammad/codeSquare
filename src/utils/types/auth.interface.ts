import { JwtPayload } from 'jsonwebtoken';
// import { IUserDocument } from '../../components/user/data-access/models/user.interface';
// import { Request } from 'express';
// import { ParamsDictionary } from 'express-serve-static-core';
// import { ParsedQs } from 'qs';
export interface tokenSchema {
  user: {
    userId: string;
    name: string;
  };
}


// export type AuthRequest = Request & User;
// export interface AuthedRequest extends Request {
//   user: IUserDocument;
// }
// export interface AuthedRequest extends Request<ParamsDictionary, any, any, ParsedQs, Record<'user', IUserDocument>>{}

export interface Decoded extends JwtPayload {
  user: {
    userId: string;
    name: string;
  };

}

