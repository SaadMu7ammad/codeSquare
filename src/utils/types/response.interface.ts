export interface modifiedResponse {
    user?: {
      _id: string;
      userId: string;
      userName: string;
      email: string;
    };
    token?: string;
    msg?: string;
  }