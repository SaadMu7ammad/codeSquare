import { Request } from 'express';

export interface RequestWithBody extends Request {
  email?: string,
  password?:string
}