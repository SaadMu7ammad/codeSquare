import jwt from 'jsonwebtoken';
import { tokenSchema } from './types/auth.interface';
import { Response } from 'express';
import { configENV } from '../config';

export const generateAccessToken = (payload: tokenSchema, res: Response) => {
  //  res.cookie(payload, process.env.JWT_SECRET, {
  //   expiresIn: process.env.JWT_LIFETIME,
  //  });
  const token = jwt.sign(payload, configENV.getJWT_SECRET(), {
    expiresIn: configENV.getJWT_LIFETIME(),
  });
  res.cookie('jwt', token, {
    httpOnly: true,
    secure: configENV.getJWT_LIFETIME() === 'development',
    sameSite: 'strict',
    maxAge: 3 * 24 * 60 * 60 * 1000,
  });
  return token;
};
