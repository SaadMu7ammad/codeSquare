import { NextFunction, Request, Response } from 'express';
import { CustomAPIError } from '../libraries/errors/index';

const errorHandlerMiddleware = (
  err: CustomAPIError,
  _req: Request,
  res: Response,
  _next: NextFunction
): Response => {
  if (err instanceof CustomAPIError) {
    if (!err.code || (err.code && err.code !== 11000)) {
      console.log('errorHandlerMiddleware1');
      return res.status(err.statuscode).json({
        status: err.status,
        msg: err.message,
        stack: process.env.NODE_ENV === 'production' ? null : err.stack,
      });
    }
  }
  // if (err instanceof CustomAPIError) {
  if (err.code && err.code === 11000) {
    console.log(err);
    return res.status(500).json({
      statusCode: 500,
      msg: `${err?.keyValue?.email} cannot be duplicated for registration`,
      stack: process.env.NODE_ENV === 'production' ? null : err.stack,
    });
  }
  if (err.value && err.name === 'CastError') {
    console.log('CastError');
    console.log(err.value);
    console.log('--------');
    console.log(err);
    return res.status(500).json({
      statusCode: 500,
      msg: `${err.value._id || err.value} not a valid id for tasks`,
      stack: process.env.NODE_ENV === 'production' ? null : err.stack,
    });
  }
  console.log(err);
  return res.status(500).json({
    statusCode: 500,
    msg: err.message || err.value,
    stack: process.env.NODE_ENV === 'production' ? null : err.stack,
  });
};

export { errorHandlerMiddleware };
