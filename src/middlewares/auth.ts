import jwt from 'jsonwebtoken';
import { Response, NextFunction, Request } from 'express';
import { Decoded } from '../utils/types/auth.interface';
import { UnauthenticatedError } from '../libraries/errors/index';
import UserModel from '../components/user/data-access/models/user.model';
import { configENV } from '../config';

export const auth = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (req && req?.headers && req?.headers?.cookie) {
      // throw new customError.UnauthenticatedError('no token found');
      const authHeader = req.headers.cookie;
      const token = authHeader.split('=')[1];
      // if (!token || token == 'null') {
      if (!token) {
        throw new UnauthenticatedError('no token found');
      }
      const payload = jwt.verify(token, configENV.getJWT_SECRET()) as Decoded;
      if (payload.user && payload.user.userId) {
        const user = await UserModel.findById(payload.user.userId);
        if (!user) throw new UnauthenticatedError('user not found');
        res.locals.user = user;
        next();

      }
    } else {
      throw new UnauthenticatedError('not token found');
    }
  } catch (err) {
    next(err);
  }
};
