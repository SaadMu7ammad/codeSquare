import { Request, Response } from 'express';

const notFoundMiddleware = (_req: Request, res: Response) =>
  res.status(404).send('Route does not exist');

export { notFoundMiddleware };
