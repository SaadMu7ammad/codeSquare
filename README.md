# codeSquare




# API Documentation

### Register User

POST /users/register: Register a new user.
### Login User

POST /users/login: Log in a user.
### Logout User

POST /users/logout: Log out the authenticated user.
### User Profile
Get User Profile

GET /users/profile: Retrieve the profile of the authenticated user.
### Update User Profile

PUT /users/update-profile: Update the profile of the authenticated user.
Posts
### Create Post

POST /posts/create: Create a new post.
### Get All Posts

GET /posts/: Retrieve all posts with pagination and sorting.
### Get Post by ID

GET /posts/:id: Retrieve a post by its ID.
### Delete Post by ID

DELETE /posts/:id: Delete a post by its ID.
Post Comments
### Create Comment

POST /post/comments/create/:postId: Create a comment for a specific post.
### Edit Comment

PUT /post/comments/edit/:commentId: Edit a comment.
### Delete Comment

DELETE /post/comments/del/:commentId: Delete a comment.
Reactions
### Like Post
POST /post/reaction/like/:postId: Like a post.
